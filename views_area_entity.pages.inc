<?php

/**
 * @file
 * Admin functions to list, edit and delete.
 */

/**
 * Simple listing.
 */
function views_area_entity_admin_listing() {
  $has_da = module_exists('domain');
  $header = array();
  $header[] = array('data' => t('Title'), 'field' => 'va.title', 'sort' => 'asc');
  $header[] = array('data' => t('View'), 'field' => 'human_name');
  $header[] = array('data' => t('Display'), 'field' => 'display_title');
  $header[] = array('data' => t('Type'), 'field' => 'va.type');
  if ($has_da) {
    $header[] = array('data' => t('Domain'), 'field' => 'sitename');
  }
  $header[] = array('data' => t('Operations'));

  $query = db_select('views_area_entity', 'va')->extend('PagerDefault')->extend('TableSort');
  $query->fields('va', array('vaid', 'type', 'title'));
  $query->distinct();
  $query->addExpression("COALESCE(v.human_name, 'MISSING VIEW')", 'human_name');
  $query->addExpression("COALESCE(d.display_title, 'MISSING DISPLAY')", 'display_title');
  $query->addExpression("va.domain_id", 'assigned_domain_id');

  $query->leftJoin('views_view', 'v', 'va.view = v.name');
  $query->leftJoin('views_display', 'd', 'd.id = va.display');
  if ($has_da) {
    $query->fields('da', array('domain_id'));
    $query->leftJoin('domain', 'da', 'da.domain_id = va.domain_id');
    $query->addExpression("COALESCE(da.sitename, 'UNKNOWN / MISSING')", 'sitename');
  }
  $result = $query
    ->orderByHeader($header)
    ->limit(50)
    ->execute();

  $rows = array();
  $destination = drupal_get_destination();
  $types = array(
    'empty' => t('Empty'),
    'header' => t('Header'),
    'footer' => t('Footer'),
  );
  foreach ($result as $data) {
    $row = array();
    $row['data']['title'] = check_plain($data->title);
    $row['data']['human_name'] = check_plain($data->human_name);
    $row['data']['display_title'] = check_plain($data->display_title);
    $row['data']['type'] = $types[$data->type];
    if ($has_da) {
      if ($data->assigned_domain_id == 0) {
        $row['data']['sitename'] = t('All affiliates');
      }
      else {
        $row['data']['sitename'] = empty($data->domain_id) || empty($data->sitename) ? t('UNKNOWN / MISSING') : check_plain($data->sitename);
      }
    }

    $operations = array();
    $operations['edit'] = array(
      'title' => t('edit'),
      'href' => "admin/structure/views/area-entities/$data->vaid/edit",
      'query' => $destination,
    );
    $operations['delete'] = array(
      'title' => t('delete'),
      'href' => "admin/structure/views/area-entities/$data->vaid/delete",
      'query' => $destination,
    );
    $row['data']['operations'] = array(
      'data' => array(
        '#theme' => 'links',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline', 'nowrap')),
      ),
    );

    $rows[] = $row;
  }

  $build['path_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No fieldable view areas have been created.'),
  );
  $build['path_pager'] = array('#theme' => 'pager');

  return $build;
}

/**
 * Menu callback; Display an area form.
 */
function views_area_entity_admin_page($action, $area_or_view_name, $display = 'default', $type = 'empty', $domain_id = 0) {

  if ($action == 'add') {
    drupal_set_title(t('Add views area'), PASS_THROUGH);
    $area = entity_get_controller('views_area_entity')->create();
    $area->view = $area_or_view_name;
    $area->display = $display;
    $area->type = $type;
    $area->domain_id = $domain_id;
    $area->title = $area->view . ' (' . $display . ') for ' . $type;
    if ($domain_id) {
      $area->title .= ': Domain ID ' . $domain_id;
    }
  }
  else {
    $area = $area_or_view_name;
  }
  return drupal_get_form('views_area_entity_admin_form', $area);
}

/**
 * Area administration form.
 */
function views_area_entity_admin_form($form, &$form_state, $area) {
  $form['#area'] = $area;

  $form['vaid'] = array('#type' => 'value', '#value' => empty($area->vaid) ? NULL : $area->vaid);
  $form['view'] = array('#type' => 'value', '#value' => $area->view);
  $form['display'] = array('#type' => 'value', '#value' => $area->display);
  $form['type'] = array('#type' => 'value', '#value' => $area->type);
  $form['domain_id'] = array('#type' => 'value', '#value' => $area->domain_id);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $area->title,
    '#weight' => -5,
  );
  field_attach_form('views_area_entity', $area, $form, $form_state);

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  if (!empty($area->vaid)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      //'#access' => views_area_entity_access('delete', $area),
      '#submit' => array('views_area_entity_admin_form_delete_submit'),
    );
  }

  return $form;
}

function views_area_entity_admin_form_delete_submit($form, &$form_state) {
  drupal_goto('admin/structure/views/area-entities/' . $form['#area']->vaid . '/delete');
}

/**
 * Views area form submit callback.
 */
function views_area_entity_admin_form_submit($form, &$form_state) {
  $area = (object) $form_state['values'];
  entity_form_submit_build_entity('views_area_entity', $area, $form, $form_state);
  if (entity_get_controller('views_area_entity')->save($area) == SAVED_NEW) {
    drupal_set_message(t('Views area %title has been created.', array('%title' => $area->title)));
  }
  else {
    drupal_set_message(t('Views area %title has been updated.', array('%title' => $area->title)));
  }
}

/**
 * Menu callback; confirm deletion of a Views area.
 *
 * @ingroup forms
 * @see views_area_entity_admin_delete_submit()
 */
function views_area_entity_admin_delete($form, &$form_state, $area) {
  $form['#area'] = $area;
  return confirm_form($form,
    t('Are you sure you want to delete the Views area %title?', array('%title' => $area->title)),
    'admin/structure/views/area-entities',
    t('References that point to this Views area will become invalid. This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Process area delete form submission.
 */
function views_area_entity_admin_delete_submit($form, &$form_state) {
  $area = $form['#area'];
  // Invoke the delete() method that we added to the controller.
  entity_get_controller('views_area_entity')->delete(array($area->vaid));
  drupal_set_message(t('Views area %title has been deleted.', array('%title' => $area->title)));

  $form_state['redirect'] = 'admin/structure/views/area-entities';
}
